﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.user_id = New System.Windows.Forms.TextBox()
        Me.user_name = New System.Windows.Forms.TextBox()
        Me.user_lastname = New System.Windows.Forms.TextBox()
        Me.user_birthday = New System.Windows.Forms.DateTimePicker()
        Me.user_tel = New System.Windows.Forms.TextBox()
        Me.user_maried = New System.Windows.Forms.ComboBox()
        Me.user_placebirthday = New System.Windows.Forms.TextBox()
        Me.user_f = New System.Windows.Forms.CheckBox()
        Me.user_m = New System.Windows.Forms.CheckBox()
        Me.user_po = New System.Windows.Forms.RadioButton()
        Me.user_jo = New System.Windows.Forms.RadioButton()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 22.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.Label1.Location = New System.Drawing.Point(107, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(576, 41)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Menaxhimi i te dhenbave te nje personi"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.Label2.Location = New System.Drawing.Point(52, 87)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(25, 21)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "ID"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.Label5.Location = New System.Drawing.Point(52, 133)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(42, 21)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Emri"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.Label6.Location = New System.Drawing.Point(52, 173)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(69, 21)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Mbiemri"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.Label7.Location = New System.Drawing.Point(52, 212)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(72, 21)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Datlindja"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.Label8.Location = New System.Drawing.Point(52, 249)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(53, 21)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Nr_Tel"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.Label3.Location = New System.Drawing.Point(54, 289)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(50, 21)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Gjinia"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.Label4.Location = New System.Drawing.Point(54, 330)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(74, 21)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Punesimi"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.Label9.Location = New System.Drawing.Point(54, 374)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(123, 21)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "Statusi Martesor"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.Label10.Location = New System.Drawing.Point(54, 421)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(83, 21)
        Me.Label10.TabIndex = 11
        Me.Label10.Text = "Vendlindja"
        '
        'user_id
        '
        Me.user_id.Location = New System.Drawing.Point(307, 88)
        Me.user_id.Name = "user_id"
        Me.user_id.Size = New System.Drawing.Size(387, 23)
        Me.user_id.TabIndex = 12
        '
        'user_name
        '
        Me.user_name.Location = New System.Drawing.Point(307, 131)
        Me.user_name.Name = "user_name"
        Me.user_name.Size = New System.Drawing.Size(387, 23)
        Me.user_name.TabIndex = 13
        '
        'user_lastname
        '
        Me.user_lastname.Location = New System.Drawing.Point(307, 171)
        Me.user_lastname.Name = "user_lastname"
        Me.user_lastname.Size = New System.Drawing.Size(387, 23)
        Me.user_lastname.TabIndex = 14
        '
        'user_birthday
        '
        Me.user_birthday.Location = New System.Drawing.Point(307, 210)
        Me.user_birthday.Name = "user_birthday"
        Me.user_birthday.Size = New System.Drawing.Size(387, 23)
        Me.user_birthday.TabIndex = 15
        '
        'user_tel
        '
        Me.user_tel.Location = New System.Drawing.Point(307, 247)
        Me.user_tel.Name = "user_tel"
        Me.user_tel.Size = New System.Drawing.Size(387, 23)
        Me.user_tel.TabIndex = 16
        '
        'user_maried
        '
        Me.user_maried.FormattingEnabled = True
        Me.user_maried.Items.AddRange(New Object() {"I/E Martuar", "Beqar/e", "I/E Ve"})
        Me.user_maried.Location = New System.Drawing.Point(307, 372)
        Me.user_maried.Name = "user_maried"
        Me.user_maried.Size = New System.Drawing.Size(387, 23)
        Me.user_maried.TabIndex = 17
        '
        'user_placebirthday
        '
        Me.user_placebirthday.Location = New System.Drawing.Point(307, 419)
        Me.user_placebirthday.Name = "user_placebirthday"
        Me.user_placebirthday.Size = New System.Drawing.Size(387, 23)
        Me.user_placebirthday.TabIndex = 18
        '
        'user_f
        '
        Me.user_f.AutoSize = True
        Me.user_f.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.user_f.Location = New System.Drawing.Point(307, 293)
        Me.user_f.Name = "user_f"
        Me.user_f.Size = New System.Drawing.Size(32, 19)
        Me.user_f.TabIndex = 19
        Me.user_f.Text = "F"
        Me.user_f.UseVisualStyleBackColor = True
        '
        'user_m
        '
        Me.user_m.AutoSize = True
        Me.user_m.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.user_m.Location = New System.Drawing.Point(369, 293)
        Me.user_m.Name = "user_m"
        Me.user_m.Size = New System.Drawing.Size(37, 19)
        Me.user_m.TabIndex = 20
        Me.user_m.Text = "M"
        Me.user_m.UseVisualStyleBackColor = True
        '
        'user_po
        '
        Me.user_po.AutoSize = True
        Me.user_po.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.user_po.Location = New System.Drawing.Point(307, 333)
        Me.user_po.Name = "user_po"
        Me.user_po.Size = New System.Drawing.Size(39, 19)
        Me.user_po.TabIndex = 21
        Me.user_po.TabStop = True
        Me.user_po.Text = "Po"
        Me.user_po.UseVisualStyleBackColor = True
        '
        'user_jo
        '
        Me.user_jo.AutoSize = True
        Me.user_jo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.user_jo.Location = New System.Drawing.Point(369, 333)
        Me.user_jo.Name = "user_jo"
        Me.user_jo.Size = New System.Drawing.Size(37, 19)
        Me.user_jo.TabIndex = 22
        Me.user_jo.TabStop = True
        Me.user_jo.Text = "Jo"
        Me.user_jo.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Green
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(308, 503)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 39)
        Me.Button1.TabIndex = 23
        Me.Button1.Text = "INSERT"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Orange
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(418, 503)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 39)
        Me.Button2.TabIndex = 24
        Me.Button2.Text = "UPDATE"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Red
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(524, 503)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 39)
        Me.Button3.TabIndex = 25
        Me.Button3.Text = "Delete"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.Blue
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(626, 503)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 39)
        Me.Button4.TabIndex = 26
        Me.Button4.Text = "SEARCH"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(52, 597)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowTemplate.Height = 25
        Me.DataGridView1.Size = New System.Drawing.Size(647, 150)
        Me.DataGridView1.TabIndex = 27
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(800, 789)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.user_jo)
        Me.Controls.Add(Me.user_po)
        Me.Controls.Add(Me.user_m)
        Me.Controls.Add(Me.user_f)
        Me.Controls.Add(Me.user_placebirthday)
        Me.Controls.Add(Me.user_maried)
        Me.Controls.Add(Me.user_tel)
        Me.Controls.Add(Me.user_birthday)
        Me.Controls.Add(Me.user_lastname)
        Me.Controls.Add(Me.user_name)
        Me.Controls.Add(Me.user_id)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents user_id As TextBox
    Friend WithEvents user_name As TextBox
    Friend WithEvents user_lastname As TextBox
    Friend WithEvents user_birthday As DateTimePicker
    Friend WithEvents user_tel As TextBox
    Friend WithEvents user_maried As ComboBox
    Friend WithEvents user_placebirthday As TextBox
    Friend WithEvents user_f As CheckBox
    Friend WithEvents user_m As CheckBox
    Friend WithEvents user_po As RadioButton
    Friend WithEvents user_jo As RadioButton
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents DataGridView1 As DataGridView
End Class
