﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAppCrud
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void F_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=PCNT042;Initial Catalog=WindowsForm;Integrated Security=True;Pooling=False");
            con.Open();
            SqlCommand cmd = new SqlCommand("insert into users values (@ID,@Emri,@Mbiemri,@Datlindja,@Telefon,@Gjinia,@Puna,@StatusiMartesor,@Vendlindja)", con);
            cmd.Parameters.AddWithValue("@ID",int.Parse(textBox5.Text));
            cmd.Parameters.AddWithValue("@Emri", textBox1.Text);
            cmd.Parameters.AddWithValue("@Mbiemri", textBox2.Text);
            cmd.Parameters.AddWithValue("@Datlindja", int.Parse(dateTimePicker1.Text));
            cmd.Parameters.AddWithValue("@Telefon", int.Parse(textBox3.Text));
            cmd.Parameters.AddWithValue("@Gjinia", checkBox1.Checked);
            cmd.Parameters.AddWithValue("@Gjinia", checkBox2.Checked);
            cmd.Parameters.AddWithValue("@Puna", checkBox3.Checked);
            cmd.Parameters.AddWithValue("@Puna", checkBox4.Checked);
            cmd.Parameters.AddWithValue("@GjendjaMartesore", (textBox6.Text));
            cmd.Parameters.AddWithValue("@Vendlindja", (textBox4.Text));
            cmd.ExecuteNonQuery();

            con.Close();
            MessageBox.Show("Successfull insert");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=PCNT042;Initial Catalog=WindowsForm;Integrated Security=True;Pooling=False");
            con.Open();
            SqlCommand cmd = new SqlCommand("Update users set @ID,Emri = @Emri,Mbiemri = @Mbiemri,Datlindja = @Datlindja,Telefon = @Telefon,Gjinia = @Gjinia,Puna = @Puna,StatusiMartesor = @StatusiMartesor,Vendlindja = @Vendlindja",con);
            cmd.Parameters.AddWithValue("@ID", int.Parse(textBox5.Text));
            cmd.Parameters.AddWithValue("@Emri", textBox1.Text);
            cmd.Parameters.AddWithValue("@Mbiemri", textBox2.Text);
            cmd.Parameters.AddWithValue("@Datlindja", int.Parse(dateTimePicker1.Text));
            cmd.Parameters.AddWithValue("@Telefon", int.Parse(textBox3.Text));
            cmd.Parameters.AddWithValue("@Gjinia", checkBox1.Checked);
            cmd.Parameters.AddWithValue("@Gjinia", checkBox2.Checked);
            cmd.Parameters.AddWithValue("@Puna", checkBox3.Checked);
            cmd.Parameters.AddWithValue("@Puna", checkBox4.Checked);
            cmd.Parameters.AddWithValue("@GjendjaMartesore", (textBox6.Text));
            cmd.Parameters.AddWithValue("@Vendlindja", (textBox4.Text));
            cmd.ExecuteNonQuery();
            con.Close();
            MessageBox.Show("Successfull update");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=PCNT042;Initial Catalog=WindowsForm;Integrated Security=True;Pooling=False");
            con.Open();
            SqlCommand cmd = new SqlCommand("Delete users where ID = @ID", con);
            cmd.Parameters.AddWithValue("@ID", int.Parse(textBox5.Text));
            cmd.ExecuteNonQuery();
            con.Close();
            MessageBox.Show("Successfull delete");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=PCNT042;Initial Catalog=WindowsForm;Integrated Security=True;Pooling=False");
            con.Open();
            SqlCommand cmd = new SqlCommand("Select * from users where ID = @ID", con);
            cmd.Parameters.AddWithValue("@ID", int.Parse(textBox5.Text));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;  
        }
    }
}
